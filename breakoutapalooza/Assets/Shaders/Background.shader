﻿/*
* this shader converts audio spectrum data from a texture into bars of color
*/

Shader "Custom/Background"
{
	Properties
	{
		_MusicColor( "Music Color", Color ) = ( 1, 1, 1, 1 )
		_ScriptTexture( "Sound Spectrum from script (leave blank)", 2D ) = "white" {}
		_UVMap( "UVMap (leave blank)", 2D ) = "white" {}
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf SimpleSpecular

		struct CostumSurfaceOutput
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Specular;
			half3 GlossColor;
			half Alpha;
		};

	//simple specular, see https://docs.unity3d.com/Manual/SL-SurfaceShaderLightingExamples.html
	half4 LightingSimpleSpecular( CostumSurfaceOutput s, half3 lightDir, half3 viewDir, half atten )
		{
		half3 h = normalize( lightDir + viewDir );
		half diff = max( 0, dot( s.Normal, lightDir ) );
		float nh = max( 0, dot( s.Normal, h ) );
		float spec = pow( nh, 48.0 );
		half4 c;
		c.rgb = ( s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec ) * atten;
		c.a = s.Alpha;
		return c;
		}

		sampler2D _UVMap;
		sampler2D _ScriptTexture;
		fixed4 _MusicColor;

		struct Input
		{
			float2 uv_UVMap;
		};

		void surf( Input IN, inout CostumSurfaceOutput o )
		{
			fixed musicRawOutput = ( tex2D( _ScriptTexture, IN.uv_UVMap ).r );
			fixed musicOutput = ( ( tex2D( _UVMap, float2( ( 0 - musicRawOutput ), 0 ) ).r * lerp( 0, 1, saturate( musicRawOutput ) ) ) );
			fixed3 musicColor = fixed3( musicOutput, musicOutput, musicOutput );
			o.Albedo = ( musicColor * _MusicColor );
		}
ENDCG
	}
		Fallback "Diffuse"
}