﻿using UnityEngine;

/// <summary>
/// Manages the behaviour of the bouncer, the only element on the screen that the player actually controls
/// </summary>
public class BouncerManager : MonoBehaviour
{
    [SerializeField]
    private Vector2 m_HorizontalLimits;
    [SerializeField]
    private float m_RotationSpeed = 5f;
    [SerializeField]
    private float m_MovementSpeed = 5f;
    [SerializeField]
    [Range( 0, 90 )]
    private float m_RotationMax;

    [SerializeField]
    private AnimationCurve Acceleration;
    private float x_LastPosition;

    public void MoveHorizontally( Vector2 leftStick )
    {
        //set target position
        float xPosition = transform.position.x + leftStick.x * m_MovementSpeed;
        //apply acceleration
        float speed = xPosition - x_LastPosition;
        xPosition = x_LastPosition + speed * ( 1 + Acceleration.Evaluate( Mathf.Abs( speed ) ) );
        //smooth movement
        xPosition = Mathf.Lerp( transform.position.x, xPosition, Time.deltaTime );
        //enforce limits
        xPosition = Mathf.Clamp( xPosition, m_HorizontalLimits.x, m_HorizontalLimits.y );
        x_LastPosition = xPosition;
        //apply position
        transform.position = new Vector3( xPosition, transform.position.y, transform.position.z );
    }

    public void RotateHorizontally( Vector2 rightStick )
    {
        if ( m_RotationMax == 0 )
            return;
        //up or down should have the same effect, the important is right or left
        Vector2 forwardRotator = rightStick.y > 0 ? rightStick : new Vector2( rightStick.x, -rightStick.y );
        //use the angle between (0,0)-to-rightStick vector and (0,0)-to-(0,1) vector to calculate the rotation angle
        float zAngle = Mathf.Rad2Deg * Mathf.Acos( Vector2.Dot( forwardRotator.normalized, Vector2.right ) );
        //90 deg rotation so that forward = horizontal = 0 angle
        zAngle -= 90;
        //limit maximum rotation to +/-m_RotationMax
        zAngle *= m_RotationMax / 30;
        //apply rotation
        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            Quaternion.Euler( transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, zAngle / 3 ),
            Time.deltaTime * m_RotationSpeed );
    }
}
