﻿using System.Collections.Generic;
using UnityEngine;

public enum EGameLoopState { startState, initialization, startLevel, countDown, playLevel, exit }
public struct GameLoopSharedData
{
    public int lifesLeft;
    public int bricksLeft;
    public int score;
    public int comboScore;
    public int comboCounter;
    public int currentLevel;
    public Dictionary<BrickManager, BrickData> bricks;
}
/// <summary>
/// Manager for the main state of the game, the game loop. 
/// It will start a cycle of level-loading, countdowns and plays 
/// and set up the dependency injections for the components involved
/// It's both a state observer (like Main) for the sub-states and 
/// a StateManager (being itself a ScreenManager) since it's a
/// sub-state of Main. 
/// Also controls visibility for the game loop UI
/// </summary>
public class GameLoopScreenManager : ScreenManager, IStateObserver<EGameLoopState, GameLoopSharedData>
{
    private StateManager<EGameLoopState, GameLoopSharedData> m_GameLoopStateManager;
    [SerializeField]
    private GameStateReactionManager m_CountDownManager;
    [SerializeField]
    private GameStateReactionManager m_StartLevelManager;
    [SerializeField]
    private GameStateReactionManager m_PlayLevelManager;
    [SerializeField]
    private GameStateReactionManager m_BallManager;
    [SerializeField]
    private GameStateReactionManager m_GameDataVisualizer;
    [SerializeField]
    private CanvasGroup m_UIGroup;
    [SerializeField]
    private UILifesManager m_LifesVisualizer;
    [SerializeField]
    private LevelAsset m_LevelsData;
    [SerializeField]
    private SFXManager m_Sfx;
    [SerializeField]
    private MusicManager m_Music;
    [SerializeField]
    private int m_StartingLifes = 10;

    public EventCall<EGameLoopState> OnStateFinished { get { return AdvanceState; } }

    public Getter<EGameLoopState> GetObservedState { get; set; }
    public Setter<EGameLoopState> SetObservedState { get; set; }
    public Getter<GameLoopSharedData> GetObservedSharedData { get; set; }
    public Setter<GameLoopSharedData> SetObservedSharedData { get; set; }
    public EventCall<GameLoopSharedData> OnObservedSharedDataUpdate { get; }
    public GetLevelCount TotalLevelGetter { get; set; }
    public GetBrickTarget BrickTargetGetter { get; set; }

    public PlaySound OnStartGame { get; set; }
    public PlaySound OnLeveBeginning { get; set; }
    public PlaySound OnLevelComplete { get; set; }
    public PlaySound OnGameOver { get; set; }
    public PlaySound OnVictory { get; set; }

    private void Awake()
    {
        m_GameLoopStateManager = new StateManager<EGameLoopState, GameLoopSharedData>();
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, m_CountDownManager );
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, m_StartLevelManager );
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, m_PlayLevelManager );
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, m_BallManager );
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, m_GameDataVisualizer );
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, m_LifesVisualizer );
        DependencyInjectionFacade.Inject( m_GameLoopStateManager, this );
        DependencyInjectionFacade.Inject( this, m_LevelsData );
        DependencyInjectionFacade.Inject( this, m_Sfx );
        DependencyInjectionFacade.Inject( this, m_Music );
        m_UIGroup.alpha = 0;
    }

    public override void OnMainStateUpdate( EMainState newState, EMainState oldState )
    {
        base.OnMainStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EMainState.gameLoop: Show(); break;
        }
    }

    protected override void Load()
    {
        OnStartGame();
    }

    protected override void Show()
    {
        base.Show();
        SetObservedSharedData( new GameLoopSharedData()
        {
            lifesLeft = m_StartingLifes,
            bricksLeft = 1,
            score = 0,
            currentLevel = 0,
            bricks = new Dictionary<BrickManager, BrickData>()
        } );
        SetObservedState( EGameLoopState.initialization );
        RequestLock( this );
        m_UIGroup.alpha = 1;
    }

    protected override void Hide()
    {
        base.Hide();
        m_UIGroup.alpha = 0;
    }

    public void AdvanceState( EGameLoopState oldState )
    {
        //main states are traversed sequentially
        if ( oldState == EGameLoopState.playLevel )
            EvaluateEnding();
        else if ( oldState == EGameLoopState.exit )
        {
            ReleaseLock( this );
        }
        else
        {
            if ( oldState + 1 == EGameLoopState.startLevel )
                OnLeveBeginning();
            SetObservedState( GetObservedState() + 1 );
        }
    }

    private void EvaluateEnding()
    {
        GameLoopSharedData observedData = GetObservedSharedData();
        if ( observedData.bricksLeft <= BrickTargetGetter( observedData.currentLevel ) )
        {
            ++observedData.currentLevel;
            SetObservedSharedData( observedData );
            if ( observedData.currentLevel < TotalLevelGetter() )
            {
                OnLeveBeginning();
                OnLevelComplete();
                SetObservedState( EGameLoopState.startLevel );
            }
            else
            {
                MainSharedData managedData = GetManagedSharedData();
                managedData.finalScore = observedData.score;
                managedData.hasWon = true;
                SetManagedSharedData( managedData );
                OnVictory();
                SetObservedState( EGameLoopState.exit );
            }
        }
        else if ( observedData.lifesLeft > 0 )
            SetObservedState( EGameLoopState.countDown );
        else
        {
            MainSharedData managedData = GetManagedSharedData();
            managedData.finalScore = observedData.score;
            managedData.hasWon = false;
            SetManagedSharedData( managedData );
            OnGameOver();
            SetObservedState( EGameLoopState.exit );
        }
    }
}
