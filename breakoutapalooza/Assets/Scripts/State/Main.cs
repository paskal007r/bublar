﻿using UnityEngine;

public enum EMainState { startState, initialization, startScreen, gameLoop, endScreen, exit }
public struct MainSharedData
{
    public float finalScore;
    public bool hasWon;
}

/// <summary>
/// this is meant to be the highest-level game manager and state observer
/// all the game loop starts here.
/// </summary>
public class Main : MonoBehaviour, IStateObserver<EMainState, MainSharedData>
{
    [SerializeField]
    private InputManager m_Input;
    [SerializeField]
    private BouncerManager m_Bouncer;
    [SerializeField]
    private StartScreenManager m_StartScreen;
    [SerializeField]
    private ScreenManager m_EndScreen;
    [SerializeField]
    private ScreenManager m_GameLoopScreen;
    private StateManager<EMainState, MainSharedData> m_MainGameStateManager;
    public EventCall<EMainState> OnStateFinished { get { return AdvanceState; } }

    public Getter<MainSharedData> GetObservedSharedData { get; set; }
    public Setter<MainSharedData> SetObservedSharedData { get; set; }
    public Getter<EMainState> GetObservedState { get; set; }
    public Setter<EMainState> SetObservedState { get; set; }

    public EventCall<MainSharedData> OnObservedSharedDataUpdate { get; }

    private void Awake()
    {
        m_MainGameStateManager = new StateManager<EMainState, MainSharedData>();
        DependencyInjectionFacade.Inject( m_Bouncer, m_Input );
        DependencyInjectionFacade.Inject( m_StartScreen, m_Input );
        DependencyInjectionFacade.Inject( m_MainGameStateManager, m_StartScreen );
        DependencyInjectionFacade.Inject( m_MainGameStateManager, m_EndScreen );
        DependencyInjectionFacade.Inject( m_MainGameStateManager, m_GameLoopScreen );
        DependencyInjectionFacade.Inject( m_MainGameStateManager, this );
        SetObservedSharedData( new MainSharedData() { finalScore = 0f } );
    }

    public void Start()
    {
        SetObservedState( EMainState.initialization );
    }

    public void AdvanceState( EMainState oldState )
    {
        //main states are traversed sequentially
        if ( oldState != EMainState.exit )
            SetObservedState( GetObservedState() + 1 );
        else
            Application.Quit();
    }
}
