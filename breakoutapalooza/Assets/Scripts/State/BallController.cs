﻿using UnityEngine;

/// <summary>
/// Manages Ball's behaviour, it's responsible for signaling impacts,
/// manages also the Ball position and physics, adding extra speed on bricks hit
/// resetting speed on a bouncer hit, enforcing max speed.
/// </summary>
public class BallController : GameStateReactionManager
{
    public OnColliderEvent OnBouncerCollision { get; set; }
    [SerializeField]
    private Transform m_BouncerRestPosition;
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private float m_InitialForceIntensity;
    [SerializeField]
    private TriggerListener m_ImpactListener;
    [SerializeField]
    private SFXManager m_Sfx;

    [SerializeField]
    [Range( 0f, 100f )]
    private float m_BrickSpeedIncrease = 5;

    [SerializeField]
    private float m_MaxSpeed = 17.5f;

    public PlaySound OnGenericHit { get; set; }

    protected void Awake()
    {
        DependencyInjectionFacade.Inject( this, m_ImpactListener );
        DependencyInjectionFacade.Inject( this, m_Sfx );
    }

    public override void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        base.OnGameStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EGameLoopState.countDown: ResetBallPosition(); break;
            case EGameLoopState.playLevel: ThrowBall(); break;
        }
    }


    private void ResetBallPosition()
    {
        transform.SetParent( m_BouncerRestPosition );
        m_Rigidbody.velocity = Vector3.zero;
        transform.localPosition = Vector3.zero;
    }
    private void ThrowBall()
    {
        transform.SetParent( null );
        m_Rigidbody.AddForce( m_BouncerRestPosition.up * m_InitialForceIntensity );
    }

    public void OnCollisionHit( Collision collision )
    {
        var brick = collision.transform.GetComponent<BrickManager>();
        var bouncer = collision.transform.GetComponent<BouncerManager>();
        if ( brick )
            OnBrickHit( brick );
        else if ( bouncer )
        {
            OnBouncerHit( bouncer );
            OnBouncerCollision( collision );
        }
        else
            OnGenericHit();
    }

    private void OnBrickHit( BrickManager brick )
    {
        m_Rigidbody.AddForce( m_Rigidbody.velocity.normalized * m_BrickSpeedIncrease / 100f, ForceMode.VelocityChange );
        m_Rigidbody.velocity = m_Rigidbody.velocity.normalized * Mathf.Min( m_MaxSpeed, m_Rigidbody.velocity.magnitude );
    }

    private void OnBouncerHit( BouncerManager brick )
    {
        var previousDirection = m_Rigidbody.velocity.normalized;
        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.AddForce( previousDirection * m_InitialForceIntensity );
    }
}

