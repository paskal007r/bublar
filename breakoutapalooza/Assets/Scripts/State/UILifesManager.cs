﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// lifes visualization manager, it will show to the player the
/// current amount of lifes and animate the list.
/// also supports gaining lifes, but I didn't end up using the feature due to time constraints.
/// </summary>
public class UILifesManager : GameStateReactionManager
{
    [SerializeField]
    private GameObject m_ElementPrefab;
    [SerializeField]
    private CanvasGroup m_GroupAlpha;
    private List<GameObject> m_List = new List<GameObject>();
    public override EventCall<GameLoopSharedData> OnManagedSharedDataUpdate { get { return UpDateData; } }

    public override void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        base.OnGameStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EGameLoopState.initialization: Show(); break;
            case EGameLoopState.exit: Hide(); break;
        }
    }

    protected override void Load()
    {
        m_List.Clear();
        var managedData = GetManagedSharedData();
        for ( int i = 0 ; i < managedData.lifesLeft ; i++ )
        {
            Push( false );
        }
    }

    protected override void Show()
    {
        m_GroupAlpha.alpha = 1;
        SweepList();
    }

    protected override void Hide()
    {
        m_GroupAlpha.alpha = 0;
    }

    protected void UpDateData( GameLoopSharedData data )
    {
        if ( data.lifesLeft != m_List.Count )
        {
            int difference = m_List.Count - data.lifesLeft;
            if ( difference > 0 )//lifes lost
            {
                for ( int i = 0 ; i < difference ; i++ )
                {
                    Pop();
                }
            }
            else//lifes gained
            {
                for ( int i = 0 ; i < difference ; i++ )
                {
                    Push( true );
                }
            }
        }
    }

    public void Pop()
    {
        var life = m_List [ m_List.Count - 1 ];
        life.transform.localScale = Vector3.zero;
        life.GetComponent<Image>().DOFade( 0f, 0.2f );
        Sequence introAnimation = DOTween.Sequence();
        introAnimation.Append( life.transform.DOScale( 2f, 0.21f ) );
        introAnimation.AppendCallback( delegate ()
        {
            m_List.Remove( life );
            Destroy( life );//oh no! Thanos plz staph!
        } );
        introAnimation.Play();
    }

    public void Push( bool animate )
    {
        var life = Instantiate( m_ElementPrefab, transform );
        m_List.Add( life );
        if ( animate )
        {
            life.transform.localScale = Vector3.zero;
            Sequence introAnimation = DOTween.Sequence();
            introAnimation.Append( life.transform.DOScale( 2f, 0.2f ) );
            introAnimation.Append( life.transform.DOScale( .8f, 0.2f ) );
            introAnimation.Append( life.transform.DOScale( 1.2f, 0.2f ) );
            introAnimation.Append( life.transform.DOScale( 1f, 0.2f ) );
            introAnimation.Play();
        }
    }
    private void SweepList()
    {
        float totalTime = 0;
        Sequence introAnimation = DOTween.Sequence();
        foreach ( var life in m_List )
        {
            Sequence lifeAnimation = DOTween.Sequence();
            life.transform.localScale = Vector3.zero;
            lifeAnimation.Append( life.transform.DOScale( 2f, 0.2f ) );
            lifeAnimation.Append( life.transform.DOScale( 1f, 0.2f ) );
            introAnimation.Insert( totalTime, lifeAnimation );
            totalTime += 0.2f;
        }
        introAnimation.Play();
    }
}
