﻿using UnityEngine;

public delegate void BrickInitializer( BrickManager brick, BrickData data );
/// <summary>
/// This state manager is responsible for loading the levels by grabbing
/// data from the level asset scriptableobject, using the brick pools
/// The level data is received as a matrix of BrickData
/// </summary>
public class StartLevelManager : GameStateReactionManager
{
    [SerializeField]
    private LevelAsset m_LevelData;
    [SerializeField]
    private Transform m_BrickParent;
    [SerializeField]
    private Vector2 m_BrickSize;
    [SerializeField]
    private Pool [] m_BrickPool;

    public GetBricks BrickGetter { get; set; }
    private void Start()
    {
        DependencyInjectionFacade.Inject( this, m_LevelData );
        foreach ( var pool in m_BrickPool )
        {
            pool.Populate();
        }
    }

    public override void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        base.OnGameStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EGameLoopState.startLevel: LoadNewLevel(); break;
        }
    }

    public BrickInitializer InitializeBrick { get; set; }

    private void LoadNewLevel()
    {
        var managedData = GetManagedSharedData();
        managedData.bricksLeft = 0;
        var leftOvers = m_BrickParent.GetComponentsInChildren<BrickManager>();
        foreach ( var brick in leftOvers )
        {
            brick.Dismiss();
        }
        managedData.bricks.Clear();
        var brickData = BrickGetter( managedData.currentLevel );
        for ( int i = 0 ; i < brickData.GetLength( 0 ) ; i++ )
        {
            for ( int j = 0 ; j < brickData.GetLength( 1 ) ; j++ )
            {
                Vector3 position = new Vector3( j * m_BrickSize.x, i * m_BrickSize.y, 0 );
                var currentBrickData = brickData [ i, j ];
                if ( currentBrickData.prefabReference )
                {
                    var pool = FindPool( currentBrickData.prefabReference );
                    var spawned = pool.Allocate().GetComponent<BrickManager>();
                    spawned.transform.SetParent( m_BrickParent );
                    spawned.transform.localPosition = position;
                    managedData.bricks.Add( spawned, currentBrickData );
                    DependencyInjectionFacade.Inject( spawned, this );
                    InitializeBrick( spawned, currentBrickData );
                    ++managedData.bricksLeft;
                }
            }
        }

        SetManagedSharedData( managedData );
    }

    private Pool FindPool( GameObject prefabReference )
    {
        var id = prefabReference.GetHashCode();
        foreach ( var pool in m_BrickPool )
        {
            if ( pool.GetReference().GetHashCode() == id )
                return pool;
        }
        return null;
    }
}
