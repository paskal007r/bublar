﻿using UnityEngine;

/// <summary>
/// like Screenmanager, in hindsight could have done both with the same class, refactor opportunity here
/// </summary>
public abstract class GameStateReactionManager : MonoBehaviour, IStateManaged<EGameLoopState, GameLoopSharedData>
{
    public LockToggle RequestLock { get; set; }
    public LockToggle ReleaseLock { get; set; }
    public ChangeDataEvent<EGameLoopState> OnStateUpdate { get { return OnGameStateUpdate; } }

    public Getter<EGameLoopState> GetManagedState { get; set; }
    public Getter<GameLoopSharedData> GetManagedSharedData { get; set; }
    public Setter<GameLoopSharedData> SetManagedSharedData { get; set; }

    public virtual EventCall<GameLoopSharedData> OnManagedSharedDataUpdate { get; }

    public virtual void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        switch ( newState )
        {
            case EGameLoopState.initialization: Load(); break;
            case EGameLoopState.exit: Unload(); break;
        }
    }
    protected virtual void Load()
    {
        gameObject.SetActive( true );
    }
    protected virtual void Show() { }
    protected virtual void Hide() { }
    protected virtual void Unload()
    {
        gameObject.SetActive( false );
    }
}
