﻿using System.Collections;
using TMPro;
using UnityEngine;

/// <summary>
/// Manages the countdown part of the game loop
/// will happen before starting a new level or after losing a life
/// </summary>
public class CountDownManager : GameStateReactionManager
{
    [SerializeField]
    private TextMeshProUGUI m_CountDownLabel;
    [SerializeField]
    private int m_CountDownDuration;
    private int m_CountDownSoFar;

    public override void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        base.OnGameStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EGameLoopState.countDown: StartCoroutine( CountDown() ); break;
        }
    }

    private IEnumerator CountDown()
    {
        m_CountDownSoFar = 0;
        m_CountDownLabel.CrossFadeAlpha( 1, .2f, false );
        RequestLock( this );
        do
        {
            m_CountDownLabel.text = ( m_CountDownDuration - m_CountDownSoFar ).ToString();
            ++m_CountDownSoFar;
            yield return new WaitForSeconds( 1 );
        } while ( m_CountDownSoFar < m_CountDownDuration );
        m_CountDownLabel.text = "PLAY!";
        m_CountDownLabel.CrossFadeAlpha( 0, 1f, false );
        yield return new WaitForSeconds( 0.5f );
        ReleaseLock( this );
    }
}
