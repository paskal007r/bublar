﻿using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEngine;
#endif

public delegate void ChangeDataEvent<T>( T newState, T oldState );
public delegate void EventCall<T>( T oldState );
public delegate void LockToggle( object locker );
public delegate U Getter<U>();
public delegate void Setter<U>( U toSet );
/// <summary>
/// this template is used to manage a dependency-injection compatible state machine
/// this will be the primary means of managing the game's state
/// The state machine will also have a shared data object that will be made accessible
/// to all the observers and managed classes.
/// The difference between a manag class and an observer is that the managed classes should
/// only have behaviours reacting to a state change while the observer can drive the state change
/// a more practical difference is that only the observers will be notified that
/// all locks have been released for a state and only an observer will have access to
/// the SetState function, while the managed classes will be passively informed
/// of a state change having happened. Both will have access to shared data.
/// A managed class or an observer can request a lock to prevent the state being
/// signaled as "finished" until the lock is reeleased, this way coordination
/// can be handled without direct dependencies
/// </summary>
/// <typeparam name="T">states enum</typeparam>
/// <typeparam name="U">shared data</typeparam>
public class StateManager<T, U>
{
    private T m_State;
    private U m_SharedData;
    private List<object> m_Lockers = new List<object>();
    public ChangeDataEvent<T> OnStateUpdate;
    public EventCall<U> OnSharedDataUpdate;
    public EventCall<T> OnStateFinished;

    public U GetSharedData() { return m_SharedData; }

    public void SetSharedData( U value )
    {
        m_SharedData = value;
        if ( OnSharedDataUpdate != null )
            OnSharedDataUpdate( m_SharedData );
    }

    public T GetState() { return m_State; }

    public void SetState( T value )
    {
        if ( m_State.Equals( value ) )
            return;
        T oldState = m_State;
        m_State = value;
        OnStateUpdate( m_State, oldState );
#if UNITY_EDITOR
        Debug.Log( "state[" + typeof( T ).ToString() + "] " + oldState.ToString() + "->" + m_State.ToString() );
#endif
        if ( m_Lockers.Count == 0 )
        {
            OnStateFinished( m_State );
        }
    }

    public void Lock( object locker )
    {
#if UNITY_EDITOR
        if ( m_Lockers.Contains( locker ) )
            Debug.LogError( "state[" + typeof( T ).ToString() + "] DOUBLE LOCK ATTEMPT ON LOCK " + locker );
#endif
        if ( m_Lockers.Contains( locker ) )
            return;
        m_Lockers.Add( locker );
    }

    public void Unlock( object locker )
    {
        if ( m_Lockers.Contains( locker ) )
            m_Lockers.Remove( locker );

        if ( m_Lockers.Count == 0 )
            OnStateFinished( m_State );
    }
}

public interface IStateManaged<T, U>
{
    LockToggle RequestLock { set; }
    LockToggle ReleaseLock { set; }
    ChangeDataEvent<T> OnStateUpdate { get; }
    Getter<T> GetManagedState { get; set; }
    Getter<U> GetManagedSharedData { get; set; }
    Setter<U> SetManagedSharedData { get; set; }
    EventCall<U> OnManagedSharedDataUpdate { get; }
}

public interface IStateObserver<T, U>
{
    EventCall<T> OnStateFinished { get; }
    Getter<T> GetObservedState { get; set; }
    Setter<T> SetObservedState { get; set; }
    Getter<U> GetObservedSharedData { get; set; }
    Setter<U> SetObservedSharedData { get; set; }
    EventCall<U> OnObservedSharedDataUpdate { get; }
}