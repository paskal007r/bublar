﻿using UnityEngine;

public delegate void ComboEvent();
/// <summary>
/// This class manages the behaviour of the main gameplay loop, when the player is hitting bricks
/// Wires together sounds, scores, life counting, particles, combo calculation, level completion.
/// </summary>
public class PlayLevelManager : GameStateReactionManager
{
    [SerializeField]
    private TriggerListener m_DeathTrigger;
    [SerializeField]
    private BallController m_Ball;
    [SerializeField]
    private GameDataVisualizer m_ScoreVisuazlizer;
    [SerializeField]
    private SFXManager m_Sfx;
    [SerializeField]
    private LevelAsset m_LevelData;
    [SerializeField]
    private ParticleManager m_Particles;

    public ComboEvent OnComboStart { get; set; }
    public ComboEvent OnComboIncrease { get; set; }
    public ComboEvent OnComboSuccess { get; set; }
    public ComboEvent OnComboFail { get; set; }
    public GetBrickTarget BrickTargetGetter { get; set; }
    public ParticleRequest EmitParticles { get; set; }

    private void Awake()
    {
        DependencyInjectionFacade.Inject( this, m_DeathTrigger );
        DependencyInjectionFacade.Inject( this, m_Ball );
        DependencyInjectionFacade.Inject( this, m_Sfx );
        DependencyInjectionFacade.Inject( this, m_LevelData );
        DependencyInjectionFacade.Inject( this, m_Particles );
        DependencyInjectionFacade.Inject( m_ScoreVisuazlizer, this );
    }

    public override void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        base.OnGameStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EGameLoopState.playLevel: StartLevel(); break;
        }
    }

    private void StartLevel()
    {
        RequestLock( this );
        var managedData = GetManagedSharedData();
        foreach ( var brick in managedData.bricks )
        {
            DependencyInjectionFacade.Inject( this, brick.Key );
        }
    }

    public void OnBrickDestroyed( BrickManager brick, Vector3 position )
    {
        EmitParticles( position, EParticleType.destroy );
        var managedData = GetManagedSharedData();
        ++managedData.comboCounter;
        managedData.comboScore += managedData.bricks [ brick ].destroyScore * ( managedData.comboCounter );
        managedData.bricks.Remove( brick );
        --managedData.bricksLeft;
        if ( managedData.bricksLeft <= BrickTargetGetter( managedData.currentLevel ) )
        {
            managedData.score += managedData.comboScore;
            managedData.comboCounter = 0;
            managedData.comboScore = 0;
            SetManagedSharedData( managedData );
            ReleaseLock( this );
        }
        else
        {
            SetManagedSharedData( managedData );
            ComboHit( managedData.comboCounter );
        }
    }

    public void OnBrickHit( BrickManager brick, Vector3 position )
    {
        EmitParticles( position, EParticleType.hit );
        var managedData = GetManagedSharedData();
        ++managedData.comboCounter;
        managedData.comboScore += managedData.bricks [ brick ].hitScore * ( managedData.comboCounter );
        SetManagedSharedData( managedData );
        ComboHit( managedData.comboCounter );
    }

    public void OnDeath( Collider trigger )
    {
        var managedData = GetManagedSharedData();
        managedData.lifesLeft -= 1;
        OnComboFail();
        managedData.comboCounter = 0;
        managedData.comboScore = 0;
        SetManagedSharedData( managedData );
        ReleaseLock( this );
    }

    public void OnBouncerHit( Collision collision )
    {
        //combo end
        var managedData = GetManagedSharedData();
        managedData.score += managedData.comboScore;
        OnComboSuccess();
        managedData.comboScore = 0;
        managedData.comboCounter = 0;
        SetManagedSharedData( managedData );
    }

    private void ComboHit( int comboCount )
    {
        if ( comboCount > 1 )
            OnComboIncrease();
        else
            OnComboStart();
    }
}