﻿using UnityEngine;

/// <summary>
/// this class has the boilerplate code shared by all screen managers to implement StateManaged interface 
/// and sets the basic behaviour that they will share such as Loading on initialization
/// </summary>
public abstract class ScreenManager : MonoBehaviour, IStateManaged<EMainState, MainSharedData>
{
    public LockToggle RequestLock { get; set; }
    public LockToggle ReleaseLock { get; set; }
    public Getter<EMainState> GetManagedState { get; set; }
    public Setter<EMainState> SetManagedState { get; set; }
    public Getter<MainSharedData> GetManagedSharedData { get; set; }
    public Setter<MainSharedData> SetManagedSharedData { get; set; }
    public EventCall<MainSharedData> OnManagedSharedDataUpdate { get; }

    public ChangeDataEvent<EMainState> OnStateUpdate { get { return OnMainStateUpdate; } }

    public virtual void OnMainStateUpdate( EMainState newState, EMainState oldState )
    {
        switch ( newState )
        {
            case EMainState.initialization: Load(); break;
            case EMainState.exit: Unload(); break;
        }
    }
    protected virtual void Load()
    {
        gameObject.SetActive( true );
    }
    protected virtual void Show() { }
    protected virtual void Hide() { }
    protected virtual void Unload()
    {
        gameObject.SetActive( false );
    }
}
