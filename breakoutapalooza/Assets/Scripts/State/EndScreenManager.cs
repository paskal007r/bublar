﻿using TMPro;
using UnityEngine;

/// <summary>
/// Manages the end-screen message, communicates to the player
/// whether it's a win or a loss
/// </summary>
public class EndScreenManager : ScreenManager
{
    [SerializeField]
    private TextMeshProUGUI m_WinningStatus;
    [SerializeField]
    private TextMeshProUGUI m_FinalScore;
    [SerializeField]
    private TextMeshProUGUI m_InputPrompt;
    [SerializeField]
    private InputManager m_Input;

    private void Awake()
    {
        DependencyInjectionFacade.Inject( this, m_Input );
    }

    public override void OnMainStateUpdate( EMainState newState, EMainState oldState )
    {
        base.OnMainStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EMainState.endScreen: Show(); break;
        }
    }

    protected override void Load()
    {
        base.Load();
        m_FinalScore.CrossFadeAlpha( 0, 0, true );
        m_WinningStatus.CrossFadeAlpha( 0, 0, true );
        m_InputPrompt.CrossFadeAlpha( 0, 0, true );
    }

    protected override void Show()
    {
        var managedData = GetManagedSharedData();
        m_FinalScore.CrossFadeAlpha( 1, 1, true );
        m_WinningStatus.CrossFadeAlpha( 1, 1, true );
        m_InputPrompt.CrossFadeAlpha( 1, 1, true );
        m_FinalScore.text = managedData.finalScore.ToString();
        m_WinningStatus.text = managedData.hasWon ? "A winner is YOU!\n~Pro Wrestling" : "All your base are belong to us!\n~Zero Wing";
        m_InputPrompt.text = "press Button A to quit!";
        RequestLock( this );
    }

    public void OnEndPressed()
    {
        ReleaseLock( this );
    }
}
