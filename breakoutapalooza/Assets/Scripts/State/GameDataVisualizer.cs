﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class dedicated to show the game's main loop shared data to the player
/// includes: combo status and score, earned score, current level, bricks left
/// </summary>
public class GameDataVisualizer : GameStateReactionManager
{
    [SerializeField]
    private TextMeshProUGUI m_BricksLeft;
    [SerializeField]
    private TextMeshProUGUI m_Score;
    [SerializeField]
    private TextMeshProUGUI m_LevelCounter;
    [SerializeField]
    private TextMeshProUGUI m_ComboCounter;
    [SerializeField]
    private Image m_ExclamationMarkIcon;
    [SerializeField]
    private TextMeshProUGUI m_ComboScore;
    [SerializeField]
    private Animator m_ComboCounterAnimator;
    [SerializeField]
    private Animator m_ComboScoreAnimator;
    [SerializeField]
    private LevelAsset m_LevelData;
    private float m_LastComboCounter;
    private float m_LastComboScore;
    public GetBrickTarget BrickTargetGetter { get; set; }

    public override EventCall<GameLoopSharedData> OnManagedSharedDataUpdate { get { return UpDateData; } }

    private void Awake()
    {
        DependencyInjectionFacade.Inject( this, m_LevelData );
    }

    public override void OnGameStateUpdate( EGameLoopState newState, EGameLoopState oldState )
    {
        base.OnGameStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EGameLoopState.startLevel: ResetCounterAnimatorState(); break;
            case EGameLoopState.playLevel: Show(); break;
            case EGameLoopState.exit: Hide(); break;
        }
    }

    private void ResetCounterAnimatorState()
    {
        m_ComboCounterAnimator.SetTrigger( "reset" );
        m_ComboScoreAnimator.SetTrigger( "reset" );
        m_ComboCounterAnimator.ResetTrigger( "start" );
        m_ComboScoreAnimator.ResetTrigger( "start" );
        m_ComboCounterAnimator.ResetTrigger( "success" );
        m_ComboScoreAnimator.ResetTrigger( "success" );
        m_ComboCounterAnimator.ResetTrigger( "hit" );
        m_ComboScoreAnimator.ResetTrigger( "hit" );
        m_ComboCounterAnimator.ResetTrigger( "fail" );
        m_ComboScoreAnimator.ResetTrigger( "fail" );
    }

    protected override void Load()
    {
        base.Load();
        m_Score.CrossFadeAlpha( 0, 0, false );
        m_LevelCounter.CrossFadeAlpha( 0, 0, false );
        m_ComboCounter.CrossFadeAlpha( 0, 0, false );
        m_ComboScore.CrossFadeAlpha( 0, 0, false );
        m_ExclamationMarkIcon.CrossFadeAlpha( 0, 0, false );

    }

    protected override void Show()
    {
        m_Score.CrossFadeAlpha( 1, 1, false );
        m_LevelCounter.CrossFadeAlpha( 1, 1, false );
        m_ComboCounter.CrossFadeAlpha( 1, 1, false );
        m_ComboScore.CrossFadeAlpha( 1, 1, false );
        m_ExclamationMarkIcon.CrossFadeAlpha( 1, 1, false );
    }

    protected override void Hide()
    {
        m_Score.CrossFadeAlpha( 0, 1, false );
        m_LevelCounter.CrossFadeAlpha( 0, 1, false );
        m_ComboCounter.CrossFadeAlpha( 0, 1, false );
        m_ComboScore.CrossFadeAlpha( 0, 1, false );
        m_ExclamationMarkIcon.CrossFadeAlpha( 0, 1, false );
    }

    protected void UpDateData( GameLoopSharedData data )
    {
        m_Score.text = data.score.ToString();
        m_LevelCounter.text = data.currentLevel.ToString();
        m_BricksLeft.text = ( data.bricksLeft - BrickTargetGetter( data.currentLevel ) ).ToString();
    }

    private void OnComboUpdate( string triggerId )
    {
        var managedData = GetManagedSharedData();
        m_ComboCounter.text = managedData.comboCounter.ToString();
        m_ComboScore.text = managedData.comboScore.ToString();
        Color comboColor;
        switch ( managedData.comboCounter )
        {
            case 0: comboColor = Color.clear; break;
            case 1: comboColor = Color.white; break;
            case 2: comboColor = Color.green; break;
            case 3: comboColor = Color.red; break;
            case 4: comboColor = Color.magenta; break;
            default: comboColor = Color.yellow; break;
        }
        m_ComboCounter.CrossFadeColor( comboColor, 0.2f, false, false );
        m_ComboScore.CrossFadeColor( comboColor, 0.2f, false, false );
        m_ExclamationMarkIcon.CrossFadeColor( comboColor, 0.2f, false, false );
        m_ComboCounterAnimator.SetTrigger( triggerId );
        m_ComboScoreAnimator.SetTrigger( triggerId );
    }

    public void OnComboStart()
    {
        OnComboUpdate( "start" );
        m_ComboCounterAnimator.ResetTrigger( "success" );
        m_ComboScoreAnimator.ResetTrigger( "success" );
        m_ComboCounterAnimator.ResetTrigger( "hit" );
        m_ComboScoreAnimator.ResetTrigger( "hit" );
        m_ComboCounterAnimator.ResetTrigger( "fail" );
        m_ComboScoreAnimator.ResetTrigger( "fail" );
    }

    public void OnComboIncrease()
    {
        OnComboUpdate( "hit" );
    }

    public void OnComboSuccess()
    {
        OnComboUpdate( "success" );
    }

    public void OnComboFail()
    {
        OnComboUpdate( "fail" );
    }
}
