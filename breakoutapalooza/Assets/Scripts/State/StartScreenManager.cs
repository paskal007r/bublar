﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class managing the initial messages to the player
/// this includes the icons explanation, the controls explanation
/// and the request for pressing the start button
/// </summary>
public class StartScreenManager : ScreenManager
{
    [SerializeField]
    private TextMeshProUGUI m_Label;
    [SerializeField]
    private TextMeshProUGUI m_ControlsLeft;
    [SerializeField]
    private Image m_ControlsLeftIcon;
    [SerializeField]
    private TextMeshProUGUI m_ControlsRight;
    [SerializeField]
    private Image m_ControlsRightIcon;
    [SerializeField]
    private CanvasGroup m_Legenda;
    private bool m_IsIntroDone;

    public override void OnMainStateUpdate( EMainState newState, EMainState oldState )
    {
        base.OnMainStateUpdate( newState, oldState );
        switch ( newState )
        {
            case EMainState.startScreen: Show(); break;
        }
    }

    protected override void Load()
    {
        base.Load();
        m_Label.DOFade( 0, 0 );
        m_ControlsLeft.DOFade( 0, 0 );
        m_ControlsRight.DOFade( 0, 0 );
        m_ControlsLeftIcon.DOFade( 0, 0 );
        m_ControlsRightIcon.DOFade( 0, 0 );
        m_Legenda.DOFade( 0, 0 );
    }

    protected override void Show()
    {
        m_Label.text = "press button A to start!";
        m_ControlsLeft.text = "Left stick to control position";
        m_ControlsRight.text = "Right stick to control rotation";
        RequestLock( this );
        FadeAnimation( 1, 1, 3, delegate () { m_IsIntroDone = true; } );
    }

    protected override void Hide()
    {
        m_Label.CrossFadeAlpha( 0, 1f, false );
        FadeAnimation( 0, 0, 1, ReleaseAfterCrossFade );
    }

    public void OnStartPressed()
    {
        if ( m_IsIntroDone )
            Hide();
    }

    private void FadeAnimation( float targetAlpha, float delay, float duration, TweenCallback callback )
    {
        Sequence introAnimation = DOTween.Sequence();
        introAnimation.Insert( delay, m_ControlsLeft.DOFade( targetAlpha, 1 ) );
        introAnimation.Insert( delay, m_ControlsLeftIcon.DOFade( targetAlpha, 1 ) );
        introAnimation.Insert( delay + duration * 0.5f, m_ControlsRight.DOFade( targetAlpha, 1 ) );
        introAnimation.Insert( delay + duration * 0.5f, m_ControlsRightIcon.DOFade( targetAlpha, 1 ) );
        introAnimation.Insert( delay + duration, m_Label.DOFade( targetAlpha, 1 ) );
        introAnimation.Insert( delay + duration, m_Legenda.DOFade( targetAlpha, 1 ) );

        if ( callback != null )
            introAnimation.AppendCallback( callback );
        introAnimation.Play();
    }

    private void ReleaseAfterCrossFade()
    {
        ReleaseLock( this );
    }
}
