﻿using UnityEngine;

public delegate void OnBrickEvent( BrickManager brick, Vector3 position );
/// <summary>
/// Controller for the brick, counts hits, signals collisions and implements IPoolable interface
/// </summary>
public class BrickManager : MonoBehaviour, IPoolable
{
    [SerializeField]
    private TriggerListener m_Trigger;
    private int m_Hits;

    private void Awake()
    {
        DependencyInjectionFacade.Inject( this, m_Trigger );
    }

    public void InitializeBrick( BrickManager brick, BrickData data )
    {
        brick.Initialize( data );
    }

    public void Initialize( BrickData data )
    {
        m_Hits = data.hits;
    }

    public void OnHit( Collision trigger )
    {
        --m_Hits;
        var position = transform.position;
        if ( m_Hits <= 0 )
        {
            Dismiss();
            OnBrickDestroyed( this, position );
        }
        else
        {
            OnBrickHit( this, position );
        }
    }

    public void Dismiss()
    {
        Source.PutInPool( gameObject );
    }

    public OnBrickEvent OnBrickHit { get; set; }
    public OnBrickEvent OnBrickDestroyed { get; set; }
    public Pool Source { private get; set; }
}
