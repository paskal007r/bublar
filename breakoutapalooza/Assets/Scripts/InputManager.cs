using UnityEngine;

public delegate void buttonReaction();
public delegate void axisEffect( Vector2 axisVal );
/// <summary>
/// input manager class, mainly serves as an interface for dependency injection
/// </summary>
public class InputManager : MonoBehaviour
{
    private string m_XButtonName = "Fire3";
    private string m_YButtonName = "Fire2";
    private string m_AButtonName = "Fire1";
    private string m_BButtonName = "Jump";
    private string m_LeftStickHorizontalName = "Horizontal";
    private string m_LeftStickVerticalName = "Vertical";
    private string m_RightStickHorizontalName = "rightStickHorizontal";
    private string m_RightStickVerticalName = "rightStickVertical";
    private string m_LeftTriggerName = "leftTrigger";
    private string m_RightTriggerName = "rightTrigger";

    [SerializeField]
    private float m_TriggerSensibility = 0.2f;
    // Use this for initialization

    public buttonReaction OnXButtonPress = delegate () { };
    public buttonReaction OnYButtonPress = delegate () { };
    public buttonReaction OnAButtonPress = delegate () { };
    public buttonReaction OnBButtonPress = delegate () { };
    public buttonReaction OnXButtonPressContinuous = delegate () { };
    public buttonReaction OnYButtonPressContinuous = delegate () { };
    public buttonReaction OnAButtonPressContinuous = delegate () { };
    public buttonReaction OnBButtonPressContinuous = delegate () { };
    public axisEffect OnLeftStick = delegate ( Vector2 a ) { };
    public axisEffect OnRightStick = delegate ( Vector2 a ) { };
    public buttonReaction OnLeftTriggerPressContinuous = delegate () { };
    public buttonReaction OnRightTriggerPressContinuous = delegate () { };
    public System.Action OnInputStartRead = delegate () { };

    private void Update()
    {
        OnInputStartRead();
        if ( Input.GetButtonDown( m_XButtonName ) )
        { OnXButtonPress(); }
        if ( Input.GetButtonDown( m_YButtonName ) )
        { OnYButtonPress(); }
        if ( Input.GetButtonDown( m_AButtonName ) )
        { OnAButtonPress(); }
        if ( Input.GetButtonDown( m_BButtonName ) )
        { OnBButtonPress(); }
        if ( Input.GetButton( m_XButtonName ) )
        { OnXButtonPressContinuous(); }
        if ( Input.GetButton( m_YButtonName ) )
        { OnYButtonPressContinuous(); }
        if ( Input.GetButton( m_AButtonName ) )
        { OnAButtonPressContinuous(); }
        if ( Input.GetButton( m_BButtonName ) )
        { OnBButtonPressContinuous(); }
        if ( Input.GetAxis( m_LeftTriggerName ) > m_TriggerSensibility )
        { OnLeftTriggerPressContinuous(); }
        if ( Input.GetAxis( m_RightTriggerName ) > m_TriggerSensibility )
        { OnRightTriggerPressContinuous(); }
        OnLeftStick( new Vector2( Input.GetAxis( m_LeftStickHorizontalName ), Input.GetAxis( m_LeftStickVerticalName ) ) );
        OnRightStick( new Vector2( Input.GetAxis( m_RightStickHorizontalName ), Input.GetAxis( m_RightStickVerticalName ) ) );
    }
}
