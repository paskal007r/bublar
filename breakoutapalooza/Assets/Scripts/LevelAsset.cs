﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LevelData
{
    public Texture2D colorCode; //texture representing the level
    public int brickTarget; //when this many bricks are left, the level is won
}
[System.Serializable]
public struct BrickColorPair
{
    public Color colorCode; //when this code is in the texture, the brick is spawned
    public BrickData brick;
}
[System.Serializable]
public struct BrickData
{
    public GameObject prefabReference; //brick prefab to spawn
    public int hitScore; //how many points per hit
    public int destroyScore; //how many points when destroying this type of brick
    public int hits; //how many hits to destroy the brick
}
public delegate BrickData [,] GetBricks( int level );
public delegate int GetLevelCount();
public delegate int GetBrickTarget( int level );

/// <summary>
/// READ ONLY class mantainig all the data needed to construct levels
/// this includes the list of levels encoded as textures
/// </summary>
[CreateAssetMenu( fileName = "Levels", menuName = "Level Data" )]
public class LevelAsset : ScriptableObject
{
    [SerializeField]
    private LevelData [] m_LevelsData;
    public LevelData [] LevelsData { get { return m_LevelsData; } }
    [SerializeField]
    private List<BrickColorPair> m_BrickData = new List<BrickColorPair>();
    public List<BrickColorPair> BrickData { get { return m_BrickData; } }

    /// <summary>
    /// convert the texture for the specified level into a matrix of brickdata
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public BrickData [,] GetLevelBricks( int level )
    {
        if ( level >= m_LevelsData.Length )
            return null;

        Texture2D currentLevel = m_LevelsData [ level ].colorCode;
        //get all the pixels
        Color [] codedData = currentLevel.GetPixels( 0, 0, currentLevel.width, currentLevel.height );
        //initialize result buffer
        BrickData [,] result = new BrickData [ currentLevel.width, currentLevel.height ];
        //create a brick dictionary to speed up the search for a Brickdata matching the Color
        Dictionary<int, BrickData> brickData = new Dictionary<int, BrickData>();
        foreach ( BrickColorPair item in m_BrickData )
        {
            brickData.Add( ColorUtility.ToHtmlStringRGB( item.colorCode ).GetHashCode(), item.brick );
        }
        //for every pixel, get the matching BrickData
        for ( int i = 0 ; i < currentLevel.height ; i++ )
        {
            for ( int j = 0 ; j < currentLevel.width ; j++ )
            {
                result [ i, j ] = brickData [ ColorUtility.ToHtmlStringRGB( codedData [ i * currentLevel.height + j ] ).GetHashCode() ];
            }
        }
        return result;
    }

    public int GetLevelTarget( int level )
    {
        if ( level >= m_LevelsData.Length )
            return -1;
        return m_LevelsData [ level ].brickTarget;
    }

    public int TotalLevels() { return m_LevelsData.Length; }
}
