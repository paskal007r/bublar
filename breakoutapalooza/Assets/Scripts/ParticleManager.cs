﻿using System.Collections;
using UnityEngine;

public delegate void ParticleRequest( Vector3 position, EParticleType type );
public enum EParticleType { hit, destroy }
/// <summary>
/// Service managing all the particle effects in the game, using pools
/// </summary>
public class ParticleManager : MonoBehaviour
{
    [SerializeField]
    private Pool m_HitEffects;
    [SerializeField]
    private Pool m_DestroyEffects;

    private void Start()
    {
        m_HitEffects.Populate();
        m_DestroyEffects.Populate();
    }

    public void EmitParticles( Vector3 position, EParticleType type )
    {
        GameObject spawned = null;
        switch ( type )
        {
            case EParticleType.hit:
                spawned = m_HitEffects.Allocate();
                break;
            case EParticleType.destroy:
                spawned = m_DestroyEffects.Allocate();
                break;
            default:
                break;
        }
        if ( spawned )
        {
            ParticleSystem effect = spawned.GetComponent<ParticleSystem>();
            spawned.transform.SetParent( transform );
            effect.transform.position = position;
            effect.Play();
            StartCoroutine( DelayedDispose( spawned.GetComponent<IPoolable>() ) );
        }
    }

    private IEnumerator DelayedDispose( IPoolable effect )
    {
        yield return new WaitForSeconds( 1.5f );
        effect.Dismiss();
    }
}
