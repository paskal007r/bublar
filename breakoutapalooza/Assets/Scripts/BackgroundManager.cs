﻿using UnityEngine;

/// <summary>
/// Class responsible for using the Background shader, 
/// by passing the sound spectrum data to the bg material
/// </summary>
public class BackgroundManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource m_MusicSource;
    [SerializeField]
    private float m_Multiplier = 1;
    private Material m_BackgroundMaterial;
    private Texture2D m_SpectrumMap;
    private float [] m_SpectrumBuffer = new float [ 256 ];

    private void Awake()
    {
        m_BackgroundMaterial = GetComponent<Renderer>().material;
        m_SpectrumMap = new Texture2D( m_SpectrumBuffer.Length, 1, TextureFormat.RGBA32, false );
        m_BackgroundMaterial.SetTexture( "_ScriptTexture", m_SpectrumMap );
    }

    private void Update()
    {
        m_MusicSource.GetSpectrumData( m_SpectrumBuffer, 0, FFTWindow.BlackmanHarris );
        for ( int i = 0 ; i < m_SpectrumBuffer.Length ; i++ )
        {
            m_SpectrumMap.SetPixel( i, 1, new Color( m_SpectrumBuffer [ i ] * 255.0f * m_Multiplier, 0, 0, 0 ) );
        }
        m_SpectrumMap.Apply();
    }
}
