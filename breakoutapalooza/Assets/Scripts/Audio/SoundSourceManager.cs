﻿using UnityEngine;

public delegate void PlaySound();
/// <summary>
/// base class for dealing with an audio source
/// </summary>
public abstract class SoundSourceManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource m_Source;

    protected void PlayClip( AudioClip clip )
    {
        m_Source.clip = clip;
        m_Source.Play();
    }
}
