﻿using UnityEngine;

/// <summary>
/// Manager for the SFX in the game, the purpose is to delegate the 
/// clip-to-event association to a Dependency injection controller
/// </summary>
public class SFXManager : SoundSourceManager
{
    [SerializeField]
    private AudioClip m_Die1;
    public void Die1()
    {
        PlayClip( m_Die1 );
    }

    [SerializeField]
    private AudioClip m_Die2;
    public void Die2()
    {
        PlayClip( m_Die2 );
    }

    [SerializeField]
    private AudioClip m_Explosion0;
    public void Explosion0()
    {
        PlayClip( m_Explosion0 );
    }

    [SerializeField]
    private AudioClip m_Explosion1;
    public void Explosion1()
    {
        PlayClip( m_Explosion1 );
    }

    [SerializeField]
    private AudioClip m_Explosion2;
    public void Explosion2()
    {
        PlayClip( m_Explosion2 );
    }

    [SerializeField]
    private AudioClip m_Helicopter;
    public void Helicopter()
    {
        PlayClip( m_Helicopter );
    }

    [SerializeField]
    private AudioClip m_Jump1;
    public void Jump1()
    {
        PlayClip( m_Jump1 );
    }

    [SerializeField]
    private AudioClip m_Jump2;
    public void Jump2()
    {
        PlayClip( m_Jump2 );
    }

    [SerializeField]
    private AudioClip m_Morse;
    public void Morse()
    {
        PlayClip( m_Morse );
    }

    [SerializeField]
    private AudioClip m_Pickup1;
    public void Pickup1()
    {
        PlayClip( m_Pickup1 );
    }

    [SerializeField]
    private AudioClip m_Pickup2;
    public void Pickup2()
    {
        PlayClip( m_Pickup2 );
    }

    [SerializeField]
    private AudioClip m_Pickup3;
    public void Pickup3()
    {
        PlayClip( m_Pickup3 );
    }

    [SerializeField]
    private AudioClip m_Shot1;
    public void Shot1()
    {
        PlayClip( m_Shot1 );
    }

    [SerializeField]
    private AudioClip m_Shot2;
    public void Shot2()
    {
        PlayClip( m_Shot2 );
    }

    [SerializeField]
    private AudioClip m_Shot3;
    public void Shot3()
    {
        PlayClip( m_Shot3 );
    }

    [SerializeField]
    private AudioClip m_Warning;
    public void Warning()
    {
        PlayClip( m_Warning );
    }
}
