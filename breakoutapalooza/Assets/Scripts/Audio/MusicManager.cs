﻿using UnityEngine;

/// <summary>
/// Manager for the music in the game, the purpose is to delegate the 
/// clip-to-event association to a Dependency injection controller
/// </summary>
public class MusicManager : SoundSourceManager
{
    [SerializeField]
    private AudioClip m_Aggressive;
    public void Aggressive()
    {
        PlayClip( m_Aggressive );
    }

    [SerializeField]
    private AudioClip m_Quiet;
    public void Quiet()
    {
        PlayClip( m_Quiet );
    }

    [SerializeField]
    private AudioClip m_Soft;
    public void Soft()
    {
        PlayClip( m_Soft );
    }

    [SerializeField]
    private AudioClip m_Stage1;
    public void Stage1()
    {
        PlayClip( m_Stage1 );
    }

    [SerializeField]
    private AudioClip m_Title;
    public void Title()
    {
        PlayClip( m_Title );
    }
}
