﻿public static class LevelAssetDIC
{
    public static void Load( StartLevelManager target, LevelAsset service )
    {
        target.BrickGetter += service.GetLevelBricks;
    }
    public static void Load( GameLoopScreenManager target, LevelAsset service )
    {
        target.TotalLevelGetter += service.TotalLevels;
        target.BrickTargetGetter += service.GetLevelTarget;
    }
    public static void Load( PlayLevelManager target, LevelAsset service )
    {
        target.BrickTargetGetter += service.GetLevelTarget;
    }
    public static void Load( GameDataVisualizer target, LevelAsset service )
    {
        target.BrickTargetGetter += service.GetLevelTarget;
    }
}
