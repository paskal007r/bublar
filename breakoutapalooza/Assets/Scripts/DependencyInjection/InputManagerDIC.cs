public static class InputManagerDIC
{
    public static void Load( BouncerManager target, InputManager input )
    {
        input.OnLeftStick += target.MoveHorizontally;
        input.OnRightStick += target.RotateHorizontally;
    }
    public static void Load( StartScreenManager target, InputManager input )
    {
        input.OnAButtonPress += target.OnStartPressed;
    }
    public static void Load( EndScreenManager target, InputManager input )
    {
        input.OnAButtonPress += target.OnEndPressed;
    }
}
