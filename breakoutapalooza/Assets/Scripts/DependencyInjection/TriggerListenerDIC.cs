﻿public static class TriggerListenerDIC
{
    public static void Load( PlayLevelManager observer, TriggerListener target )
    {
        target.OnEnter += observer.OnDeath;
    }
    public static void Load( BrickManager observer, TriggerListener target )
    {
        target.OnHit += observer.OnHit;
    }
    public static void Load( BallController observer, TriggerListener target )
    {
        target.OnHit += observer.OnCollisionHit;
    }
}
