﻿/// <summary>
/// Dependency Injection access point / facade
/// all dependencies should be implemented as a static function in the format Inject(A user, B service)
/// this class is therefore a map of all the interactions in the game code
/// Since we're using dependency injection controllers the order of user-service is more semantic than an actual
/// code dependency direction technically the DIC will depend on both classes and that's it.
/// </summary>
public static class DependencyInjectionFacade
{
    public static void Inject( PlayLevelManager user, ParticleManager service )
    {
        ParticleManagerDIC.Load( user, service );
    }

    internal static void Inject( BallController user, SFXManager service )
    {
        SoundSourceManagerDIC.Load( user, service );
    }

    internal static void Inject( GameLoopScreenManager user, SFXManager service )
    {
        SoundSourceManagerDIC.Load( user, service );
    }

    internal static void Inject( GameLoopScreenManager user, MusicManager service )
    {
        SoundSourceManagerDIC.Load( user, service );
    }

    internal static void Inject( PlayLevelManager user, SFXManager service )
    {
        SoundSourceManagerDIC.Load( user, service );
    }

    public static void Inject( BouncerManager user, InputManager service )
    {
        InputManagerDIC.Load( user, service );
    }

    public static void Inject( EndScreenManager user, InputManager service )
    {
        InputManagerDIC.Load( user, service );
    }

    public static void Inject( StartScreenManager user, InputManager service )
    {
        InputManagerDIC.Load( user, service );
    }

    public static void Inject( PlayLevelManager user, TriggerListener service )
    {
        TriggerListenerDIC.Load( user, service );
    }

    public static void Inject( BrickManager user, TriggerListener service )
    {
        TriggerListenerDIC.Load( user, service );
    }

    public static void Inject( BallController user, TriggerListener service )
    {
        TriggerListenerDIC.Load( user, service );
    }

    public static void Inject( PlayLevelManager user, BrickManager service )
    {
        PlayerLevelManagerDIC.Load( user, service );
    }

    public static void Inject( PlayLevelManager user, BallController service )
    {
        PlayerLevelManagerDIC.Load( user, service );
    }

    public static void Inject( GameDataVisualizer user, PlayLevelManager service )
    {
        PlayerLevelManagerDIC.Load( user, service );
    }

    public static void Inject( BrickManager user, StartLevelManager service )
    {
        BrickManagerDIC.Load( user, service );
    }

    internal static void Inject( StartLevelManager user, LevelAsset service )
    {
        LevelAssetDIC.Load( user, service );
    }

    internal static void Inject( GameLoopScreenManager user, LevelAsset service )
    {
        LevelAssetDIC.Load( user, service );
    }

    internal static void Inject( PlayLevelManager user, LevelAsset service )
    {
        LevelAssetDIC.Load( user, service );
    }

    internal static void Inject( GameDataVisualizer user, LevelAsset service )
    {
        LevelAssetDIC.Load( user, service );
    }

    public static void Inject<T, U>( StateManager<T, U> user, IStateManaged<T, U> service )
    {
        StateManagerDIC<T, U>.Load( user, service );
    }

    public static void Inject<T, U>( StateManager<T, U> user, IStateObserver<T, U> service )
    {
        StateObserverDIC<T, U>.Load( user, service );
    }
    /*
     * template to copy
    public static void Inject(A user, B service)
    {
        DIC.Load(user, service);
    }
    */
}
