﻿public static class StateObserverDIC<T, U>
{
    public static void Load( StateManager<T, U> target, IStateObserver<T, U> observer )
    {
        observer.GetObservedState = target.GetState;
        observer.SetObservedState = target.SetState;
        observer.GetObservedSharedData = target.GetSharedData;
        observer.SetObservedSharedData = target.SetSharedData;
        target.OnStateFinished += observer.OnStateFinished;
        target.OnSharedDataUpdate += observer.OnObservedSharedDataUpdate;
    }
}
