﻿public static class ParticleManagerDIC
{
    public static void Load( PlayLevelManager target, ParticleManager service )
    {
        target.EmitParticles += service.EmitParticles;
    }
}
