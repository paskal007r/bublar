﻿public static class PlayerLevelManagerDIC
{
    public static void Load( PlayLevelManager user, BrickManager service )
    {
        //NB: = NOT +=!!!!
        service.OnBrickHit = user.OnBrickHit;
        //NB: = NOT +=!!!!
        service.OnBrickDestroyed = user.OnBrickDestroyed;
    }
    public static void Load( PlayLevelManager user, BallController service )
    {
        //NB: = NOT +=!!!!
        service.OnBouncerCollision = user.OnBouncerHit;
    }
    public static void Load( GameDataVisualizer user, PlayLevelManager service )
    {
        service.OnComboStart += user.OnComboStart;
        service.OnComboIncrease += user.OnComboIncrease;
        service.OnComboSuccess += user.OnComboSuccess;
        service.OnComboFail += user.OnComboFail;
    }
}
