﻿
public static class SoundSourceManagerDIC
{
    public static void Load( GameLoopScreenManager target, MusicManager service )
    {
        target.OnStartGame += service.Title;
        target.OnLeveBeginning += service.Aggressive;
        target.OnGameOver += service.Quiet;
        target.OnVictory += service.Stage1;
    }
    public static void Load( BallController target, SFXManager service )
    {
        target.OnGenericHit += service.Pickup2;
    }
    public static void Load( PlayLevelManager target, SFXManager service )
    {
        target.OnComboStart += service.Shot1;
        target.OnComboIncrease += service.Shot2;
        target.OnComboSuccess += service.Shot3;
        target.OnComboFail += service.Die1;
    }
    public static void Load( GameLoopScreenManager target, SFXManager service )
    {
        target.OnGameOver += service.Die2;
        target.OnLevelComplete += service.Jump2;
        target.OnVictory += service.Explosion2;
    }
}
