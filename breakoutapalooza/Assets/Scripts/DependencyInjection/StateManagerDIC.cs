﻿public static class StateManagerDIC<T, U>
{
    public static void Load( StateManager<T, U> target, IStateManaged<T, U> managed )
    {
        managed.GetManagedState = target.GetState;
        managed.GetManagedSharedData = target.GetSharedData;
        managed.SetManagedSharedData = target.SetSharedData;
        target.OnStateUpdate += managed.OnStateUpdate;
        managed.RequestLock = target.Lock;
        managed.ReleaseLock = target.Unlock;
        target.OnSharedDataUpdate += managed.OnManagedSharedDataUpdate;
    }
}
