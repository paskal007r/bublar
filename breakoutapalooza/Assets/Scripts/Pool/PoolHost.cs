﻿using UnityEngine;

/// <summary>
/// Singleton holding the Pooled objects as children, mainly to keep 
/// the hierarchy clean.
/// </summary>
public class PoolHost : MonoBehaviour
{
    public static PoolHost instance;

    private void Awake()
    {
        if ( instance == null )
            instance = this;
        else
            Destroy( gameObject );
    }

    public static void Hold( GameObject poolable )
    {
        poolable.transform.SetParent( instance.transform );
    }
}