﻿using UnityEngine;

/// <summary>
/// Monobehaviour barebones implementation of IPoolable interface
/// </summary>
public class SimplePoolable : MonoBehaviour, IPoolable
{
    public Pool Source { private get; set; }

    public void Dismiss()
    {
        Source.PutInPool( gameObject );
    }

}
