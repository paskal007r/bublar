﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pooling implementation that will be useable as a data type
/// inside other scripts.
/// Helps avoiding GC spikes due to instantiation and destruction of objects
/// </summary>
[System.Serializable]
public class Pool
{
    private Stack<GameObject> m_Pool;
    [SerializeField]
    private int m_InitialPoolSize;
    [SerializeField]
    private int m_ExpansionPoolSize;
    [SerializeField]
    private GameObject m_PrefabReference;

    public void Populate()
    {
        if ( m_Pool == null )
        {
            m_Pool = new Stack<GameObject>();
            ExpandPoolPopulation( m_InitialPoolSize, m_PrefabReference );
        }
    }

    private void ExpandPoolPopulation( int amount, GameObject prefabReference )
    {
        for ( int j = 0 ; j < amount ; j++ )
        {
            GameObject result = GameObject.Instantiate( prefabReference );
            result.GetComponent<IPoolable>().Source = this;
            PutInPool( result );
        }
    }

    public GameObject Allocate()
    {
        if ( m_Pool.Count <= 0 )
            ExpandPoolPopulation( m_ExpansionPoolSize, m_PrefabReference );
        GameObject result = m_Pool.Pop();
        result.SetActive( true );
        return result;
    }

    public void PutInPool( GameObject handled )
    {
        handled.SetActive( false );
        handled.transform.SetPositionAndRotation( Vector3.zero, Quaternion.identity );
        PoolHost.Hold( handled );
        m_Pool.Push( handled );
    }

    public GameObject GetReference()
    {
        return m_PrefabReference;
    }
}

public interface IPoolable
{
    Pool Source { set; }
    void Dismiss();
}