﻿using UnityEngine;

public delegate void OnTriggerEvent( Collider other );
public delegate void OnColliderEvent( Collision other );
/// <summary>
/// wrapper class for Collision management, so that they can be injected
/// </summary>
public class TriggerListener : MonoBehaviour
{
    public OnColliderEvent OnHit { get; set; }
    public OnTriggerEvent OnEnter { get; set; }

    public void OnTriggerEnter( Collider other )
    {
        if ( OnEnter != null )
            OnEnter( other );
    }

    public void OnCollisionEnter( Collision collision )
    {
        if ( OnHit != null )
            OnHit( collision );
    }
}