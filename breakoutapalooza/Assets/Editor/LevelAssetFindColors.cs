﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Here it's defined a Tool function that will gather the list of colors
/// from the level-encoding textures in LevelAsset automatically so that 
/// the rest of the data can be filled in
/// </summary>
public class LevelAssetFindColors : Editor
{
    [MenuItem( "Tools/LevelAsset/FindColors", false, 0 )]
    private static void FindColors()
    {
        //find selected level asset
        LevelAsset selected = Selection.activeObject as LevelAsset;
        if ( !selected )
        {
            Debug.LogError("SELECT THE LEVEL ASSET FILE TO ANALIZE!");
            return;
        }
        //for every level
        foreach ( var levelData in selected.LevelsData )
        {
            //get the texture
            var level = levelData.colorCode;
            //get all the pixels
            Color [] codedData = level.GetPixels( 0, 0, level.width, level.height );
            //for every pixel check if its color is in the list
            for ( int i = 0 ; i < level.height ; i++ )
            {
                for ( int j = 0 ; j < level.width ; j++ )
                {
                    bool found = false;
                    //for every brickData
                    foreach ( var element in selected.BrickData )
                    {
                        //if the colors have the same hex code, they are considered equal, avoids floating point issues
                        if ( ColorUtility.ToHtmlStringRGB( codedData [ i * level.height + j ] ) == ColorUtility.ToHtmlStringRGB( element.colorCode ) )
                        {
                            found = true;
                        }
                    }
                    if ( !found ) //this color isn't in the list, a new BrickColorPair will be added
                    {
                        Debug.Log( "NEW COLOR! " + codedData [ i * level.height + j ] + "(" + i + "," + j + ")" );
                        selected.BrickData.Add( new BrickColorPair { colorCode = codedData [ i * level.height + j ] } );
                    }
                }
            }
        }
    }
}
